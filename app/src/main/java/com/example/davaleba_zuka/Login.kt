package com.example.davaleba_zuka

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class Login : AppCompatActivity() {

    lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        auth = FirebaseAuth.getInstance()
        back_to_registration_login.setOnClickListener {
            startActivity(Intent(this,MainActivity::class.java))
        }

        login_button.setOnClickListener {
            if(email_login.text.toString().isEmpty()){
                email_login.error = "Please enter email"
            }
            if(password_login.text.toString().isEmpty()){
                password_login.error = "Please enter password"
            } else {
                val email = email_login.text.toString()
                val password = password_login.text.toString()
                auth.signInWithEmailAndPassword(email,password)
                    .addOnCompleteListener(this){task ->
                        if(task.isSuccessful){
                            val intent = Intent(this,Profile::class.java)
                            startActivity(intent)
                        } else {Toast.makeText(this,"Login Failed",Toast.LENGTH_LONG).show()}
                    }
            }
        }
    }
}
