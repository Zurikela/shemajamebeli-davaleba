package com.example.davaleba_zuka

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = FirebaseAuth.getInstance()
        setContentView(R.layout.activity_main)
        already_have_an_account.setOnClickListener {
            startActivity(Intent(this,Login::class.java))
        }

        registerBtn.setOnClickListener {
            if (email_register.text.toString().isEmpty()){
                email_register.error = "Please enter email"
            }
            if (password_register.text.toString().isEmpty()){
                password_register.error = "Please enter password"
            } else{
                val email = email_register.text.toString()
                val password = password_register.text.toString()
                auth.createUserWithEmailAndPassword(email,password)
                    .addOnCompleteListener(this){task ->
                        if(task.isSuccessful){
                            startActivity(Intent(this,Profile::class.java))
                        } else {Toast.makeText(this,"registration failed",Toast.LENGTH_LONG).show()}
                    }
            }
        }
    }
}
